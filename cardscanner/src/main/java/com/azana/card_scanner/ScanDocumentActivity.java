package com.azana.card_scanner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;

import com.azana.card_scanner.api.ApiClient;
import com.azana.card_scanner.commons.Constant;
import com.azana.card_scanner.commons.models.RequestInfoParam;
import com.azana.card_scanner.commons.models.RequestParameter;
import com.azana.card_scanner.commons.utility.ProgressDialogUtility;

import org.json.JSONArray;
import org.json.JSONObject;

import io.card.payment.CardScannerActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScanDocumentActivity extends AppCompatActivity {

    private static JSONObject requestInfoParameter = null;
    private static JSONObject requestInfoFields = null;
    private static RequestInfoParam requestInfoReponse = null;

    private static String packageName = null;

    private static int SCAN_DOCUMENT_REQUEST = 1;
    private ProgressDialogUtility progressDialogUtility = null;

    private String frontImage = "",backImage = "";
    public static String CARD_SCAN_DATA = "cardScanningData";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_document);
        progressDialogUtility = new ProgressDialogUtility(this);
        validateRequestInfoParams();
    }




    private boolean validateRequestInfoParams() {
        try {
            requestInfoFields = requestInfoParameter.getJSONObject(getString(R.string.FIELD_REQUEST_INFO));
            if(!requestInfoFields.has(getString(R.string.FIELD_LICENSE_KEY))) {
                //  goBackToPrevious("02", errorMessage.get(02));
                goBackToPrevious("90",Constant.errorMessage.get(90));
                return false;
            }

            if(!requestInfoFields.has(getString(R.string.FIELD_SCAN_SIDE))) {
                goBackToPrevious("90", Constant.errorMessage.get(90));
                return false;
            }


            if(requestInfoFields.has(getString(R.string.FIELD_SCAN_SIDE))) {
                if (requestInfoFields.getString(getString(R.string.FIELD_SCAN_SIDE)).toUpperCase().equals("FRONT")
                        || requestInfoFields.getString(getString(R.string.FIELD_SCAN_SIDE)).toUpperCase().equals("BACK")
                        || requestInfoFields.getString(getString(R.string.FIELD_SCAN_SIDE)).toUpperCase().equals("BOTH")) {
                    validateLicense();
                    return true;
                }else{
                    goBackToPrevious("90", Constant.errorMessage.get(90));
                    return false;
                }
            }


        } catch (Exception e) {
            goBackToPrevious("99", Constant.errorMessage.get(99));
        }
        return true;
    }


    private RequestParameter setLicenseKeyParameter() {
        RequestParameter requestParams = new RequestParameter();
        try {
            requestParams.requestInfo.licenseKey = requestInfoFields.getString(getString(R.string.FIELD_LICENSE_KEY));
            requestParams.requestInfo.packageID = packageName;
            return requestParams;
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }

        return null;
    }


    private void validateLicense() {
        try {
           // progressDialogUtility.showProgressDialog();
            RequestParameter licenseParam = setLicenseKeyParameter();
            Call<RequestInfoParam> faceMatchInstance = new ApiClient(Constant.BASE_URL).getService().validateLicenseKey(licenseParam);
            faceMatchInstance.enqueue(new Callback<RequestInfoParam>() {
                @Override
                public void onResponse(Call<RequestInfoParam> call, Response<RequestInfoParam> response) {
                    requestInfoReponse = response.body();
                    if (requestInfoReponse.statusInfo.status.equals(getString(R.string.SUCCESS))) {
                        startCardScanning();
                        }else{
                            goBackToPrevious("02", Constant.errorMessage.get(02));
                    }
                }
                @Override
                public void onFailure(Call<RequestInfoParam> call, Throwable t) {
             //       progressDialogUtility.hideProgressDialog();
                    goBackToPrevious("91", Constant.errorMessage.get(91));

                }
            });
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
            //progressDialogUtility.hideProgressDialog();
        }
    }

    public void startCardScanning(){
        try {
            ScanDocument scanDocument = new ScanDocument();
            scanDocument.setCardScanSide(requestInfoFields.getString(getString(R.string.FIELD_SCAN_SIDE)));
            scanDocument.startScanActivity(this,SCAN_DOCUMENT_REQUEST);
           // progressDialogUtility.hideProgressDialog();
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == SCAN_DOCUMENT_REQUEST) {
                if (resultCode == RESULT_OK) {
                    byte[] frontImageBytes = (byte[]) data.getExtras().getSerializable(CardScannerActivity.EXTRA_CARD_FRONT_SIDE_IMAGE);
                    byte[] backImageBytes = (byte[]) data.getExtras().getSerializable(CardScannerActivity.EXTRA_CARD_BACK_SIDE_IMAGE);
                    if(frontImageBytes != null) {
                        frontImage = android.util.Base64.encodeToString(frontImageBytes, Base64.DEFAULT);
                    }
                    if (backImageBytes != null) {
                        backImage = android.util.Base64.encodeToString(backImageBytes, Base64.DEFAULT);
                    }
                    sendResult();
                }else{
                    goBackToPrevious("22",Constant.errorMessage.get(22));
                }
            }
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }


    public void sendResult(){
        try {
            JSONArray cardScanPayload = new JSONArray();
            JSONObject cardScanImage = new JSONObject();
            cardScanPayload.put(createJSONObject(Constant.FRONT,frontImage));
            cardScanPayload.put(createJSONObject(Constant.BACK,backImage));
            JSONObject result = setPayload(getString(R.string.SUCCESS), "", "", cardScanPayload);
            finishActivity(RESULT_OK,result.toString());

        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }





    private void goBackToPrevious(String errorCode, String message) {
        try {
            JSONObject resultFailed = setPayload(getString(R.string.FAILURE), errorCode,message, null);
            finishActivity(RESULT_CANCELED, resultFailed.toString());
        } catch (Exception e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }

    private void finishActivity(int code,String payload){
        Intent intent = getIntent();
        intent.putExtra(CARD_SCAN_DATA, payload);
        setResult(code, intent);
        finish();
    }

    private JSONObject createJSONObject(String fieldName, String data)  {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(getString(R.string.NAME), fieldName);
            jsonObject.put(getString(R.string.VALUE), data);
            return jsonObject;
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
        return jsonObject;
    }

    private JSONObject setPayload(String status, String errorCode, String errorDescription, JSONArray data) {
        JSONObject _status = new JSONObject();
        JSONObject payload = new JSONObject();
        try {
            _status.put(Constant.STATUS, status);
            _status.put(Constant.ERROR_CODE, errorCode);
            _status.put(Constant.ERROR_DESCRIPTION, errorDescription);

            payload.put(Constant.STATUS_INFO, _status);
            payload.put(Constant.FIELDS, data);
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
        return payload;

    }


    public static void setRequestInfoParam(JSONObject requestInfoParam){
        requestInfoParameter = requestInfoParam;
    }

    public static Intent startScanDocument(Context context){
        Intent intent = new Intent(context,ScanDocumentActivity.class);
        packageName = context.getPackageName();
        return intent;
    }
}
