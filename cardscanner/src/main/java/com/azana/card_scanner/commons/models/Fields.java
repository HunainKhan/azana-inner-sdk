package com.azana.card_scanner.commons.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Fields implements Serializable {

    @SerializedName("name")
    public String name;

    @SerializedName("value")

    public String value;
}
