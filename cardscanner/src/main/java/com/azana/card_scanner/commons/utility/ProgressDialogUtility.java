package com.azana.card_scanner.commons.utility;

import android.app.Dialog;
import android.content.Context;

import com.azana.card_scanner.R;



public class ProgressDialogUtility {

    Dialog mProgressDialog = null;
     Context context;

    public ProgressDialogUtility(Context context){
        this.context = context;
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new Dialog(context, R.style.ProgressBar_Theme);
            mProgressDialog.setContentView(R.layout.view_progress_dialog);
        }
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }


   public void hideProgressDialog() {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

}
