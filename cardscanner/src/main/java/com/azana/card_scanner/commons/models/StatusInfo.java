package com.azana.card_scanner.commons.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StatusInfo  implements Serializable {
    @SerializedName("status")
    public String status;

    @SerializedName("errorCode")
    public String errorCode;

    @SerializedName("errorDesc")
    public String errorDesc;
}
