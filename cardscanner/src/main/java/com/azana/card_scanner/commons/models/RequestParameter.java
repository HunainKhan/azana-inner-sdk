package com.azana.card_scanner.commons.models;

import java.io.Serializable;

public class RequestParameter implements Serializable {

    public RequestParameter(){
        requestInfo = new RequestInfo();
    }
    public RequestInfo requestInfo;
}
