package com.azana.card_scanner;

import android.app.Activity;
import android.content.Intent;

import com.azana.card_scanner.commons.Constant;

import io.card.payment.CardConfig;
import io.card.payment.CardConfig.Builder;
import io.card.payment.CardScannerActivity;


class ScanDocument {
    private Builder cardConfig;
    public static final String CARD_FRONT_IMAGE = "cardFrontSideImage";
    public static final String CARD_BACK_IMAGE = "cardBackSideImage";

    public ScanDocument(){
        cardConfig = new Builder();
    }

   public void startScanActivity(Activity activity,int REQ_CODE) {
        startCardScan(activity,REQ_CODE);
    }

    public void setCardScanSide(String scanSide){
       if(scanSide.equals(Constant.FRONT))
           cardConfig.setCardScanSide(CardConfig.CardScanSide.FRONT);
       else if(scanSide.equals(Constant.BACK))
           cardConfig.setCardScanSide(CardConfig.CardScanSide.BACK);
       else if(scanSide.equals(scanSide))
           cardConfig.setCardScanSide(CardConfig.CardScanSide.BOTH);

    }

    public void startCardScan(Activity activity,int REQ_CODE){
        CardConfig c = cardConfig.build();
        Intent intent = new Intent(activity, CardScannerActivity.class);
        intent.putExtra(CardScannerActivity.FACEOFF_CARD_CONFIG,c);
        activity.startActivityForResult(intent,REQ_CODE);
    }


    public void flashLightEnabled(boolean enable){
        cardConfig.setIsFlashOn(enable);
    }


}
