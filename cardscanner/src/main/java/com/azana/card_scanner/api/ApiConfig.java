package com.azana.card_scanner.api;


import com.azana.card_scanner.commons.models.RequestInfoParam;
import com.azana.card_scanner.commons.models.RequestParameter;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiConfig {

    @POST("api/ValidateLicenseKey")
    Call<RequestInfoParam> validateLicenseKey(@Body RequestParameter requestParam);

}
