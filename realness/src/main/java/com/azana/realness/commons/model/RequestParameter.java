package com.azana.realness.commons.model;

import java.io.Serializable;

public class RequestParameter implements Serializable {

    public RequestParameter(){
        requestInfo = new RequestInfo();
    }
    public RequestInfo requestInfo;
}
