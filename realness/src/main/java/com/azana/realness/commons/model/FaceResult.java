package com.azana.realness.commons.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FaceResult implements Serializable {
    @SerializedName("time_used")
    public String time;

    @SerializedName("confidence")
    public double confidence = 0.0;

    @SerializedName("request_id")
    public  String requestID;

    @SerializedName("error_message")
    public String erroMessage;


}
