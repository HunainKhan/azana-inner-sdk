package com.azana.realness.commons;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class Constant {
    public static final String NAME  = "name";
    public static final String VALUE  = "value";
    public static final String STATUS  = "status";
    public static final String STATUS_INFO  = "statusInfo";
    public static final String ERROR_DESCRIPTION  = "errorDesc";
    public static final String ERROR_CODE  = "errorCode";
    public static final String FIELDS  = "fields";
    public static final String REQUEST_PARAM  = "request_param";
    public static final String REQUEST_INFO  = "requestInfo";
    public static final String LICENSE_KEY = "licenseKey";
    public static String apiFaceMatch = "https://api-us.faceplusplus.com/facepp/";
    public static String BASE_URL = "http://3.6.200.246/Apis/";


    public static final Map<Integer, String> errorMessage = new HashMap<Integer, String>() {{
        put(01,"Expired License Key");
        put(02,"Invalid License Key");
        put(11,"Invalid Country Requested");
        put(12,"Invalid Country Scanned");
        put(13,"ID Failed to Scan");

        put(21,"Customer Timeout");
        put(22,"Customer Cancel");
        put(23,"Customer Error");
        put(24,"Customer Face Removed");

        put(51,"Camera or Hardware Failed");
        put(90,"Invalid Parameters");
        put(91,"System Error"); // Network timeout in our api
        put(92,"System Error"); // Selfie capture failed
        put(93,"System Error"); // Face match ID failed
        put(94,"System Error"); // MRZ License Failed
        put(99,"System Error"); // something crashed exception

    }};

//    class  FIELD {
//
//        public static final String IDScan = "id_scan";
//        public static final String VisaScan = "visa_scan";
//        public static final String PassportScan = "passport_scan";
//        public static final String EnableGalleryScan = "enable_gallery_scan";
//        public static final String CAMERA_INCOMPATIBLE = "camera_incompatible";
//        public static final String ValidIssuingCountries = "issuingCountries";
//        public static final String Vibration = "vibration";
//        public static final String ScanContinuous = "scan_continuous";
//        public static final String ValidateCountryCode = "validate_country_code";
//        public static final String ScanFromGallery = "scan_from_gallery";
//        public static final String ToggleFlash = "toggle_flash";
//        public static final String DATE_FORMAT = "date_format";
//    }
//
//
//    class KYC {
//        public static final String Name = "givenName";
//        public static final String SurName = "surname";
//        public static final String CompleteName = "completeName";
//        public static final String DateOfBirth = "dateOfBirth";
//        public static final String Gender = "gender";
//        public static final String IDNumber = "IDNumber";
//        public static final String Nationality = "nationality";
//        public static final String IssuingCountry = "issuingCountry";
//        public static final String DocumentNumber = "documentNumber";
//        public static final String DocumentExpiry = "documentExpiry";
//
//    }

    public static String[] convertToStringArray(JSONArray jsonArray) throws JSONException {
        int arraySize = jsonArray.length();
        String[] stringArray = new String[arraySize];

        for(int i=0; i<arraySize; i++) {
            stringArray[i] = (String) jsonArray.get(i);
        }
        return stringArray;
    }


}
