package com.azana.realness.api;

import com.azana.realness.commons.model.FaceResult;
import com.azana.realness.commons.model.RequestInfoParam;
import com.azana.realness.commons.model.RequestParameter;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiConfig {
    @FormUrlEncoded
    @POST("v3/compare")
    Call<FaceResult> faceMatch(@FieldMap Map<String, String> params);


    @POST("api/ValidateLicenseKey")
    Call<RequestInfoParam> validateLicenseKey(@Body RequestParameter requestParam);

}
