package com.azana.realness;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.azana.realness.api.ApiClient;
import com.azana.realness.commons.Constant;
import com.azana.realness.commons.model.FaceResult;
import com.azana.realness.commons.model.Fields;
import com.azana.realness.commons.model.RequestInfoParam;
import com.azana.realness.commons.model.RequestParameter;
import com.azana.realness.commons.utility.ProgressDialogUtility;
import com.unikrew.faceoff.liveness.FaceoffLivenessActivity;
import com.unikrew.faceoff.liveness.LivenessConfig;
import com.unikrew.faceoff.liveness.LivenessConfigException;
import com.unikrew.faceoff.liveness.LivenessResponse;
import com.unikrew.faceoff.liveness.LivenessResponseIPC;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.azana.realness.commons.Constant.NAME;
import static com.azana.realness.commons.Constant.VALUE;


public class RealnessTestActivity extends AppCompatActivity {

    private static JSONObject requestInfoParams = null;
    private LivenessConfig.Builder livnessBuilder;
    private static final int LIVENESS_CHECK_REQUEST = 2;
    private JSONObject requestInfoFields;
    private String selfieBase64;
    private double confidenceScore = 0.0;
    private TextView tv_wait;
    private ImageView ivImageView;
    public static final String LIVENESS_DATA = "liveness_data";
    private JSONArray livenessData = new JSONArray();
    private static Context activityContext;
    ProgressDialogUtility progressDialogUtility;
    private RequestInfoParam requestInfoReponse;
    private static final int PERMISSION_CAMERA = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realness_test);
        ivImageView = findViewById(R.id.iv_selfie_image);
        progressDialogUtility = new ProgressDialogUtility(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_CAMERA);
            }
        }
        evaluateRealnessTest();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LIVENESS_CHECK_REQUEST) {
            if (resultCode == RESULT_OK && data != null) {
                int responseCode = data.getIntExtra(FaceoffLivenessActivity.LIVENESS_RESPONSE_CODE, -1);
                if (responseCode > 0) {
                    LivenessResponse livenessResponse = LivenessResponseIPC.getInstance().getLivenessResponse(responseCode);
                    if (livenessResponse.getResponse() == LivenessResponse.Response.SUCCESS) {
                        selfieBase64 = livenessResponse.getBase64EncodedFullBitmap();
                        if (selfieBase64 == null) {
                            goBackToPrevious("92", Constant.errorMessage.get(92));
                        }
                        setSelfieResult();
                        performFaceMatch();
                        sendSuccessResult();
                    } else {
                        switch (livenessResponse.getResponse().getResponseCode()) {
                            case 103: // failed to complete challenge within time limit
                                goBackToPrevious("21", Constant.errorMessage.get(21));
                                break;
                            case 104: // face lost during liveness
                                goBackToPrevious("24", Constant.errorMessage.get(24));
                                break;
                            default: // failed to verify liveness or else
                                goBackToPrevious("23", Constant.errorMessage.get(23));
                                break;
                        }
                    }
                } else {
                    goBackToPrevious("92", Constant.errorMessage.get(92));
                }
            } else {
                // Customer cancel during liveness
                goBackToPrevious("22", Constant.errorMessage.get(22));
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "permission granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    private JSONObject createJSONObject(String fieldName, String data) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(NAME, fieldName);
        jsonObject.put(VALUE, data);
        return jsonObject;
    }

    private void evaluateRealnessTest() {
        if (validateLivenessParams()) {
            checkLicenseKey();
        } else {
            goBackToPrevious("90",Constant.errorMessage.get(90));
        }
    }

    private void goBackToPrevious(String errorCode, String message) {
        try {
            JSONObject resultFailed = setPayload(getString(R.string.FAILED), errorCode,message, null);
            finishActivity(RESULT_CANCELED, resultFailed.toString());
        } catch (JSONException e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }

    }


    private boolean validateLivenessParams(){
        try {
            requestInfoFields = requestInfoParams.getJSONObject(Constant.REQUEST_INFO);
            if (!requestInfoFields.has(getString(R.string.LICENSE_Key))) {
                return false;
            }
            if (requestInfoFields.has(getString(R.string.SELFIE_CAPTURE)))
            {
                if (requestInfoFields.get(getString(R.string.SELFIE_CAPTURE)).equals("Y")
                        || requestInfoFields.get(getString(R.string.SELFIE_CAPTURE)).equals("N"))
                {
                    // keep moving to next param
                } else {
                    return false;
                }
            } else {
                return false;
            }

            if (requestInfoFields.has(getString(R.string.FACE_MATCH))) {
                if (requestInfoFields.get(getString(R.string.FACE_MATCH)).equals("Y")
                        || requestInfoFields.get(getString(R.string.FACE_MATCH)).equals("N")) {

                    if (requestInfoFields.get(getString(R.string.FACE_MATCH)).equals("Y")
                            && !requestInfoFields.has(getString(R.string.IMAGE_ID))) {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } catch (Exception e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
        return true;
    }


    private void setSelfieResult() {
        try {
            if (requestInfoFields.getString(getString(R.string.SELFIE_CAPTURE)).toUpperCase().equals("Y")) {
                livenessData.put(createJSONObject(getString(R.string.SELFIE_CAPTURE), selfieBase64));
            } else {
                livenessData.put(createJSONObject(getString(R.string.SELFIE_CAPTURE), ""));
            }
        }catch (Exception ex) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean sendSuccessResult(){
        try {
            if(requestInfoFields.getString(getString(R.string.SELFIE_CAPTURE)).toUpperCase().equals("Y")
             && selfieBase64 == null){
                return false;
            }

            if(requestInfoFields.getString(getString(R.string.FACE_MATCH)).toUpperCase().equals("Y")
              && confidenceScore  == 0.0){
                return false;
            }

            JSONObject result = setPayload(getString(R.string.SUCCESS), "", "", livenessData);
            finishActivity(RESULT_OK, result.toString());
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
        return true;
    }

    private void performFaceMatch(){
        try {
            if (requestInfoFields.getString(getString(R.string.FACE_MATCH)).toUpperCase().equals("Y")) {
                if (validateFaceMatchParams()) {
                    callFaceMatchApi();
                }else {
                    goBackToPrevious("93",Constant.errorMessage.get(93));
                }
            }else{
                livenessData.put(createJSONObject(getString(R.string.MATCH_RATIO), ""));
            }
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }


    private void finishActivity(int code, String payload) {
        Intent intent = getIntent();
        intent.putExtra(LIVENESS_DATA, payload);
        setResult(code, intent);
        finish();
    }

//    private boolean validateLicense() {
//        try {
//            requestInfoFields = requestInfoParams.getJSONObject(Constant.REQUEST_INFO);
//                try {
//                    String licenseKey = requestInfoFields.getString(getString(R.string.LICENSE_Key));
//                    if (licenseKey.equals(getString(R.string.SDK_LICENSE_KEY))) {
//                        return true;
//                    }else{
//                        Toast.makeText(this,getString(R.string.INVALID_LICENSE_KEY),Toast.LENGTH_SHORT).show();
//                    }
//
//                    // Later here our API will be called where we will pass the license
//                    // API will check the license and its validity on the server
//                    // And also return the associated MRZScanner license key
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return false;
//    }

    private void callFaceMatchApi() {
        try {
            Map<String, String> fields = new HashMap<>();
            fields.put("api_key", getFieldsValue(getString(R.string.FP_KEY)));
            fields.put("api_secret", getFieldsValue(getString(R.string.FP_Secret)));
            fields.put("image_base64_1", requestInfoFields.getString(getString(R.string.IMAGE_ID)));
            fields.put("image_base64_2", selfieBase64);
            FaceMatchApiCall(fields);
        } catch (Exception e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }

    public boolean validateFaceMatchParams() {
        if (!requestInfoFields.has(getString(R.string.IMAGE_ID))) {
            return false;
        }
        return true;
    }


    private void FaceMatchApiCall(Map<String, String> fields) {
       progressDialogUtility.showProgressDialog();
        Call<FaceResult> faceMatchInstance = new ApiClient(Constant.apiFaceMatch).getService().faceMatch(fields);
        faceMatchInstance.enqueue(new Callback<FaceResult>() {
            @Override
            public void onResponse(Call<FaceResult> call, Response<FaceResult> response) {
                try {
                    FaceResult result = response.body();
                    confidenceScore = result.confidence;
                    progressDialogUtility.hideProgressDialog();
                    livenessData.put(createJSONObject(getString(R.string.MATCH_RATIO), String.valueOf(confidenceScore)));
                    if(confidenceScore == 0.0){
                        goBackToPrevious("93",Constant.errorMessage.get(93));
                    }else{
                        sendSuccessResult();
                    }

                } catch (Exception ex) {
                    goBackToPrevious("99",Constant.errorMessage.get(99));
                }
            }
            @Override
            public void onFailure(Call<FaceResult> call, Throwable t) {
                Toast.makeText(RealnessTestActivity.this,("main::" + t.getMessage()),Toast.LENGTH_SHORT).show();

                goBackToPrevious("93",Constant.errorMessage.get(93));
            }
        });
    }

    private void checkLicenseKey(){
  //      progressDialogUtility.showProgressDialog();
        RequestParameter licenseParam = setLicenseKeyParameter();
//        licenseParam.requestInfo.packageID = "com.encorepay.sdktestapp";
        Call<RequestInfoParam> faceMatchInstance = new ApiClient(Constant.BASE_URL).getService().validateLicenseKey(licenseParam);
        faceMatchInstance.enqueue(new Callback<RequestInfoParam>() {
            @Override
            public void onResponse(Call<RequestInfoParam> call, Response<RequestInfoParam> response) {
                requestInfoReponse = response.body();
                if(requestInfoReponse.statusInfo.status.equals(getString(R.string.SUCCESS))) {
                    startRealnessTest();
    //               progressDialogUtility.hideProgressDialog();
                }else{
                    goBackToPrevious(requestInfoReponse.statusInfo.errorCode,requestInfoReponse.statusInfo.errorDesc);
                }
            }

            @Override
            public void onFailure(Call<RequestInfoParam> call, Throwable t) {
                goBackToPrevious("91",Constant.errorMessage.get(91));

            }
        });

    }


    private RequestParameter setLicenseKeyParameter() {
        RequestParameter requestParams = new RequestParameter();
        try {
            requestParams.requestInfo.licenseKey = requestInfoFields.getString(getString(R.string.LICENSE_Key));
            requestParams.requestInfo.packageID = activityContext.getPackageName();
            return requestParams;
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }

        return null;
    }


    private void startRealnessTest() {
        try {

            livnessBuilder = new LivenessConfig.Builder();
            livnessBuilder.setChallengeHaveANiceSmile()
                    .setChallengeBlinkYourEyes()
                    .setChallengeOpenYourMouth()
                    .setMaxChallenges(2);
            Intent intent = new Intent(RealnessTestActivity.this, FaceoffLivenessActivity.class);
            intent.putExtra(FaceoffLivenessActivity.FACEOFF_LIVENESS_CONFIG, livnessBuilder.build());
            startActivityForResult(intent, LIVENESS_CHECK_REQUEST);

        } catch (LivenessConfigException e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }


    public static void setRequestInfoParams(JSONObject requestInfoParam) {
        requestInfoParams = requestInfoParam;
    }

    public static Intent startRealnessActivity(Context context) {
        Intent intent = new Intent(context, RealnessTestActivity.class);
        activityContext = context;
        return intent;
    }

    private JSONObject setPayload(String status, String errorCode, String errorDescription, JSONArray data) throws JSONException {
        JSONObject _status = new JSONObject();
        _status.put(Constant.STATUS, status);
        _status.put(Constant.ERROR_CODE, errorCode);
        _status.put(Constant.ERROR_DESCRIPTION, errorDescription);

        JSONObject payload = new JSONObject();
        payload.put(Constant.STATUS_INFO, _status);
        payload.put(Constant.FIELDS, data);
        return payload;

    }

    public String getFieldsValue(String fieldName){
        for(Fields field : requestInfoReponse.fields){
            if(field.name.equals(fieldName)){
                return field.value;
            }
        }
        return null;
    }


//    public static class Liveness {
//        private static LivenessConfig.Builder builder = null;
//        public static String FACEOFF_LIVENESS_CONFIG = "faceoff_liveness_config";
//        private JSONObject requestInfoParams = null;
//
//
//
//        public Intent build(Context context) {
//            try {
//                Intent intent = new Intent(context, RealnessTestActivity.class);
//                intent.putExtra(REQUEST_INFO,requestInfoParams.toString());
//                return intent;
//            } catch (Exception e) {
//                Log.d("Liveness config", e.getMessage());
//            }
//            return null;
//        }
//    }
}
