package com.encorepay.communication.commons;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class Constant {
    public static final String NAME  = "name";
    public static final String VALUE  = "value";
    public static final String STATUS  = "status";
    public static final String STATUS_INFO  = "statusInfo";
    public static final String ERROR_DESCRIPTION  = "errorDesc";
    public static final String ERROR_CODE  = "errorCode";
    public static final String FIELDS  = "fields";
    public static final String REQUEST_PARAM  = "Request_param";
    public static final String REQUEST_INFO  = "requestInfo";


    public static final String LICENSE_KEY = "licenseKey";
    public static final String PACKAGE_NAME = "packageName";

    public static final String PICTURE = "pictures";


    public static final String FRONT = "FRONT";
    public static final String BACK = "BACK";
    public static final String BOTH = "BOTH";
    public static String BASE_URL = "http://3.6.200.246/Apis/";

    public static final Map<Integer, String> errorMessage = new HashMap<Integer, String>() {{
        put(01,"Expired License Key");
        put(02,"Invalid License Key");
        put(11,"Invalid Country Requested");
        put(12,"Invalid Country Scanned");
        put(13,"ID Failed to Scan");

        put(21,"Customer Timeout");
        put(22,"Customer Cancel");
        put(23,"Customer Error");
        put(24,"Customer Face Removed");

        put(51,"Camera or Hardware Failed");
        put(90,"Invalid Parameters");
        put(91,"System Error"); // Network timeout in our api
        put(92,"System Error"); // Selfie capture failed
        put(93,"System Error"); // Face match ID failed
        put(94,"System Error"); // MRZ License Failed
        put(99,"System Error"); // something crashed exception


    }};



    public static String[] convertToStringArray(JSONArray jsonArray) throws JSONException {
        int arraySize = jsonArray.length();
        String[] stringArray = new String[arraySize];

        for(int i=0; i<arraySize; i++) {
            stringArray[i] = (String) jsonArray.get(i);
        }
        return stringArray;
    }


}
