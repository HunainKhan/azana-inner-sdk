package com.encorepay.communication.commons.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RequestInfoParam  implements Serializable {
    @SerializedName("statusInfo")
    public StatusInfo statusInfo;

    @SerializedName("fields")
    public List<Fields> fields;

    @SerializedName("pictures")
    public List<Fields> pictures;
}
