package com.encorepay.communication.commons.models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestInfo implements Serializable {

    @SerializedName("licenseKey")
    public String  licenseKey;

    @SerializedName("packageID")
    public String packageID;


    @SerializedName("productID")
    public String productID;
}
