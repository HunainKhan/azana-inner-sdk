package com.encorepay.communication.commons.models;

import java.io.Serializable;

public class RequestParameter implements Serializable {

    public RequestParameter(){
        requestInfo = new RequestInfoParam();
    }
    public RequestInfoParam requestInfo;
}
