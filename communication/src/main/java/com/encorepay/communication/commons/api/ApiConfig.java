package com.encorepay.communication.commons.api;


import com.encorepay.communication.commons.models.RequestInfoParam;
import com.encorepay.communication.commons.models.RequestParameter;
import com.encorepay.communication.commons.models.StatusInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiConfig {

    @POST("api/SendDocument")
    Call<StatusInfo> sendDocument(@Body RequestParameter requestParam);

}
