package com.encorepay.communication;

import com.encorepay.communication.commons.Constant;
import com.encorepay.communication.commons.api.ApiClient;
import com.encorepay.communication.commons.models.Fields;
import com.encorepay.communication.commons.models.RequestInfoParam;
import com.encorepay.communication.commons.models.RequestParameter;
import com.encorepay.communication.commons.models.StatusInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Document {

    RequestInfoParam _requestInfoParam;

    public interface DocumentResponse {
        void onResponse(JSONObject response);
    }

    public void sendRequest(final DocumentResponse documentResponse) throws JSONException {
        RequestParameter parameter = new RequestParameter();
        parameter.requestInfo = _requestInfoParam;
        final JSONObject statusInfoResponse = new JSONObject();
        try {
            Call<StatusInfo> faceMatchInstance = new ApiClient(Constant.BASE_URL).getService().sendDocument(parameter);
            faceMatchInstance.enqueue(new Callback<StatusInfo>() {
                @Override
                public void onResponse(Call<StatusInfo> call, Response<StatusInfo> response) {
                    StatusInfo statusInfo = response.body();
                    try {
                        statusInfoResponse.put("statusInfo", createPayload(statusInfo.status, statusInfo.errorCode, statusInfo.errorDesc));
                    } catch (JSONException e) { }

                    if (statusInfo.status.equals("SUCCESS")) {
                        documentResponse.onResponse(statusInfoResponse);
                    } else {
                        documentResponse.onResponse(statusInfoResponse);
                    }
                }

                @Override
                public void onFailure(Call<StatusInfo> call, Throwable t) {
                    //       progressDialogUtility.hideProgressDialog();
                    try {
                        statusInfoResponse.put("statusInfo",createPayload("FAILURE","99", Constant.errorMessage.get(99)));
                        documentResponse.onResponse(statusInfoResponse);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            statusInfoResponse.put("statusInfo", createPayload("FAILURE", "99", Constant.errorMessage.get(99)));
            documentResponse.onResponse(statusInfoResponse);

        }
    }


    private List<Fields> setFields(JSONArray json_fields) throws JSONException {
        List<Fields> fields = new ArrayList<>();
        for (int i = 0; i < json_fields.length(); i++) {
            JSONObject field_param = json_fields.getJSONObject(i);
            Fields f = new Fields();
            f.name = field_param.getString(Constant.NAME);
            f.value = field_param.getString(Constant.VALUE);
            fields.add(f);
        }
        return fields;
    }

    public String setRequestInfoParam(JSONObject requestInfoParam) {
        try {
            _requestInfoParam = new RequestInfoParam();
            _requestInfoParam.statusInfo = setRequestInfo(requestInfoParam.getJSONObject(Constant.REQUEST_INFO));
            _requestInfoParam.fields = setFields(requestInfoParam.getJSONArray(Constant.FIELDS));
            _requestInfoParam.fields = setFields(requestInfoParam.getJSONArray(Constant.PICTURE));
            return "";
        } catch (Exception ex) {
            return Constant.errorMessage.get(90);
        }
    }


    public StatusInfo setRequestInfo(JSONObject requestInfo) throws JSONException {
        StatusInfo statusInfo = new StatusInfo();
        statusInfo.status = requestInfo.getString(Constant.STATUS);
        statusInfo.errorCode = requestInfo.getString(Constant.ERROR_CODE);
        statusInfo.errorDesc = requestInfo.getString(Constant.ERROR_DESCRIPTION);
        return statusInfo;
    }


    public JSONObject createPayload(String status, String errorCode, String errorDesc) throws JSONException {
        JSONObject response = new JSONObject();
            response.put("status", status);
            response.put("errorCode", errorCode);
            response.put("errorDesc", errorDesc);
            return response;
    }
}
