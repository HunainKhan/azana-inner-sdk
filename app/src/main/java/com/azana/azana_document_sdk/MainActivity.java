package com.azana.azana_document_sdk;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.azana.MRZ.Scan.ScanActivity;
import com.azana.card_scanner.ScanDocumentActivity;
import com.azana.realness.RealnessTestActivity;

import org.json.JSONArray;
import org.json.JSONObject;


public class MainActivity extends androidx.appcompat.app.AppCompatActivity {

    public Button card, liveness;
    public ImageView imageViewPerson;
    public String cardImage = null;
    public static final String CARD_FRONT_IMAGE = "cardFrontSideImage";


    //ScanDocument scanDocument;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        card = findViewById(R.id.front_card);
        liveness = findViewById(R.id.livness);
        imageViewPerson = findViewById(R.id.image_view_person);


        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONArray countries = new JSONArray();
                JSONObject requestInfoParams = new JSONObject();
                JSONObject requestParams = new JSONObject();
                try {
                    // Setting Oman as document country
                   countries.put("OMN");
                  //  countries.put("ARE");
                    // Setting up input parameters for SDK
                    requestParams.put("issuingCountries", countries);
                    requestParams.put("licenseKey","34858E56FXE56XYR");
                    requestInfoParams.put("requestInfo", requestParams);
                    Intent intent = new ScanActivity.Builder().setRequestInfoParams(requestInfoParams).build(MainActivity.this);
                    startActivityForResult(intent,600);


                }catch (Exception ex){

                }


            }
        });


        liveness.setOnClickListener(view -> {
            JSONObject livnessFace = new JSONObject();
            JSONObject livenessRequestInfo = new JSONObject();
            try {
                livnessFace.put("selfieCapture", "Y");
                livnessFace.put("faceMatch", "Y");
                livnessFace.put("licenseKey", "34858E56FXE56XYR");
                livnessFace.put("imageID", cardImage);
                livenessRequestInfo.put("requestInfo", livnessFace);




                RealnessTestActivity.setRequestInfoParams(livenessRequestInfo);
                Intent intent = RealnessTestActivity.startRealnessActivity(this);
                startActivityForResult(intent, 300);
            } catch (Exception ex) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == 100) {
                if (resultCode == RESULT_OK) {
                    JSONObject s = new JSONObject(data.getExtras().getString(ScanDocumentActivity.CARD_SCAN_DATA));

                    cardImage = s.getJSONArray("fields").getJSONObject(0).getString("value");

                    //Toast.makeText(this,kycFields.getName(),Toast.LENGTH_SHORT).show();
                } else {
                    JSONObject s = new JSONObject(data.getExtras().getString(ScanDocumentActivity.CARD_SCAN_DATA));
                }
            } else if (requestCode == 300) {
                if (resultCode == RESULT_OK) {
                    JSONObject response =  new JSONObject(data.getStringExtra(RealnessTestActivity.LIVENESS_DATA));
                    Toast.makeText(MainActivity.this,String.valueOf(response.getJSONArray("fields").length()), Toast.LENGTH_SHORT).show();

                } else {
                    JSONObject response =  new JSONObject(data.getStringExtra(RealnessTestActivity.LIVENESS_DATA));
                    Toast.makeText(MainActivity.this, response.getJSONObject("statusInfo").getString("errorDesc"), Toast.LENGTH_SHORT).show();
                }

            }else if(requestCode == 600){
                if(resultCode == RESULT_OK){
                    JSONObject response =  new JSONObject(data.getStringExtra(ScanActivity.KYC_DATA));
                    Toast.makeText(MainActivity.this,String.valueOf(response.getJSONArray("fields").length()), Toast.LENGTH_SHORT).show();

                }else{
                    JSONObject response =  new JSONObject(data.getStringExtra(ScanActivity.KYC_DATA));
                    Toast.makeText(MainActivity.this,String.valueOf(response.getJSONObject("statusInfo").getString("errorDesc")), Toast.LENGTH_SHORT).show();

                }
            }else if(requestCode == 700){
                if(requestCode == RESULT_OK){
                    JSONObject response =  new JSONObject(data.getStringExtra(ScanDocumentActivity.CARD_SCAN_DATA));
                    Toast.makeText(MainActivity.this,String.valueOf(response.getJSONArray("fields").length()), Toast.LENGTH_SHORT).show();

                }else{
                    JSONObject response =  new JSONObject(data.getStringExtra(ScanDocumentActivity.CARD_SCAN_DATA));
                    Toast.makeText(MainActivity.this,String.valueOf(response.getJSONObject("statusInfo").getString("errorDesc")), Toast.LENGTH_SHORT).show();

                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
