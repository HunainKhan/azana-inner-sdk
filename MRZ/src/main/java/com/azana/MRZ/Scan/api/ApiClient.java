package com.azana.MRZ.Scan.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ApiClient {

    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    Retrofit.Builder builder;
    private ApiConfig apiService;

    public ApiClient(String url) {
        builder = new Retrofit.Builder()
                .baseUrl(url).addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create());

        Retrofit retrofit = builder.build();
        apiService = retrofit.create(ApiConfig.class);
    }

    public ApiConfig getService() {
        return apiService;
    }
}
