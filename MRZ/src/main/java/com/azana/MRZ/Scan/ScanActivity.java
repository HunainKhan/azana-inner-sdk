package com.azana.MRZ.Scan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.azana.MRZ.R;
import com.azana.MRZ.Scan.api.ApiClient;
import com.azana.MRZ.Scan.commons.Constant;
import com.azana.MRZ.Scan.commons.models.Fields;
import com.azana.MRZ.Scan.commons.models.RequestInfoParam;
import com.azana.MRZ.Scan.commons.models.RequestParameter;
import com.azana.MRZ.Scan.commons.utility.ProgressDialogUtility;
import com.scansolutions.mrzscannerlib.MRZLicenceResultListener;
import com.scansolutions.mrzscannerlib.MRZResultModel;
import com.scansolutions.mrzscannerlib.MRZScanner;
import com.scansolutions.mrzscannerlib.MRZScannerListener;
import com.scansolutions.mrzscannerlib.ScannerType;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.azana.MRZ.Scan.commons.Constant.FIELD.CAMERA_INCOMPATIBLE;
import static com.azana.MRZ.Scan.commons.Constant.FIELD.DATE_FORMAT;
import static com.azana.MRZ.Scan.commons.Constant.FIELD.EnableGalleryScan;
import static com.azana.MRZ.Scan.commons.Constant.FIELD.IDScan;
import static com.azana.MRZ.Scan.commons.Constant.FIELD.PassportScan;
import static com.azana.MRZ.Scan.commons.Constant.FIELD.ScanFromGallery;
import static com.azana.MRZ.Scan.commons.Constant.FIELD.ToggleFlash;
import static com.azana.MRZ.Scan.commons.Constant.FIELD.ValidIssuingCountries;
import static com.azana.MRZ.Scan.commons.Constant.FIELD.Vibration;
import static com.azana.MRZ.Scan.commons.Constant.FIELD.VisaScan;
import static com.azana.MRZ.Scan.commons.Constant.NAME;
import static com.azana.MRZ.Scan.commons.Constant.VALUE;
import static com.azana.MRZ.Scan.commons.Constant.convertToStringArray;
import static com.azana.MRZ.Scan.commons.Constant.errorMessage;

;

public class ScanActivity extends AppCompatActivity implements MRZScannerListener {

    public static final String KYC_DATA = "KYCData";
    private static JSONObject requestInfoParams = null;
    private String packageName;
    private ProgressDialogUtility progressDialogUtility;
    private JSONObject requestInfoFields;
    private RequestInfoParam requestInfoReponse;
    private String[] countries = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrz);
        try {
            getSavedInstanceParams();
            progressDialogUtility = new ProgressDialogUtility(this);
            validateRequestInfoParams();
        } catch (Exception ex) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }

    @Override
    public void successfulScanWithResult(MRZResultModel mrzResultModel) {
        try {
            // Validating if a valid country ID is scanned only.
            // If not then this function close with error.
           if(!validateScannedCountry(mrzResultModel.issuingCountry)) {
               JSONArray data = new JSONArray();
               data.put(createJSONObject(Constant.KYC.Name, mrzResultModel.givenNamesReadable()));
               data.put(createJSONObject(Constant.KYC.SurName, mrzResultModel.surnamesReadable()));
               data.put(createJSONObject(Constant.KYC.CompleteName, mrzResultModel.getFullName()));
               data.put(createJSONObject(Constant.KYC.DateOfBirth, mrzResultModel.dobReadable));
               data.put(createJSONObject(Constant.KYC.Gender, mrzResultModel.sex));
               data.put(createJSONObject(Constant.KYC.IDNumber, getDocumentIDBasedOnCountry(mrzResultModel)));
               data.put(createJSONObject(Constant.KYC.Nationality, getNationality(mrzResultModel)));
               data.put(createJSONObject(Constant.KYC.IssuingCountry, mrzResultModel.issuingCountry));
               data.put(createJSONObject(Constant.KYC.DocumentNumber, mrzResultModel.documentNumber));
               data.put(createJSONObject(Constant.KYC.DocumentExpiry, mrzResultModel.expirationDateReadable));
               JSONObject payload = setPayload(getString(R.string.SUCCESS), "", "", data);
               finishActivity(RESULT_OK, payload.toString());
           }
        } catch (Exception e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }

    }

    private boolean validateRequestInfoParams() {
        try {
            if(!requestInfoFields.has(getString(R.string.FIELD_LICENSE_KEY))) {
                    goBackToPrevious("90",errorMessage.get(90));
                    return false;
            }

            if(!requestInfoFields.has(getString(R.string.FIELD_ISSUE_COUNTRIES))) {
                    goBackToPrevious("90", errorMessage.get(90));
                    return false;
            }

            validateLicense();

        } catch (Exception e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
        return true;
    }

    private boolean validateCountry(JSONObject requestParam) {
            try {
                  countries = Constant.convertToStringArray(requestParam.getJSONArray(ValidIssuingCountries));
                if (Arrays.asList(countries).contains(getString(R.string.OMN))
                        || Arrays.asList(countries).contains(getString(R.string.ARE))) {
                    return true;
                }
            } catch (Exception e) {

            }
           return false;
    }

    private boolean validateScannedCountry(String country) {
        try {
            if (!Arrays.asList(countries).contains(country)) {
                goBackToPrevious("12",errorMessage.get(12));
                return true;
            }
        } catch (Exception e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
        return false;
    }

    private void goBackToPrevious(String errorCode, String message) {
        try {
            JSONObject resultFailed = setPayload(getString(R.string.FAILURE), errorCode,message, null);
            finishActivity(RESULT_CANCELED, resultFailed.toString());
            setContentView(null);
        } catch (Exception e) {
            JSONObject resultFailed = setPayload(getString(R.string.FAILURE),"99", errorMessage.get(99), null);
            finishActivity(RESULT_CANCELED, resultFailed.toString());

        }
    }

    private void finishActivity(int code,String payload){
        Intent intent = getIntent();
        intent.putExtra(KYC_DATA, payload);
        setResult(code, intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        goBackToPrevious("22",errorMessage.get(22));
    }

    private void validateLicense() {
        try {
            progressDialogUtility.showProgressDialog();
            RequestParameter licenseParam = setLicenseKeyParameter();
            Call<RequestInfoParam> faceMatchInstance = new ApiClient(Constant.BASE_URL).getService().validateLicenseKey(licenseParam);
            faceMatchInstance.enqueue(new Callback<RequestInfoParam>() {
                @Override
                public void onResponse(Call<RequestInfoParam> call, Response<RequestInfoParam> response) {
                    requestInfoReponse = response.body();
                    if (requestInfoReponse.statusInfo.status.equals(getString(R.string.SUCCESS))) {
                        if(validateCountry(requestInfoFields)) {
                            startMRZ(getFieldsValue(getString(R.string.FIELD_MRZ)));
                        }else{
                            goBackToPrevious("11", errorMessage.get(11));
                        }
                        progressDialogUtility.hideProgressDialog();
                    } else {
                        goBackToPrevious(requestInfoReponse.statusInfo.errorCode, requestInfoReponse.statusInfo.errorDesc);
                    }
                }

                @Override
                public void onFailure(Call<RequestInfoParam> call, Throwable t) {
                    progressDialogUtility.hideProgressDialog();
                    goBackToPrevious("91", Constant.errorMessage.get(91));
                    finish();

                }
            });
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
            progressDialogUtility.hideProgressDialog();
        }
    }


    @Override
    public void successfulScanWithDocumentImage(Bitmap bitmap) {

    }

    @Override
    public void scanImageFailed() {
    }

    @Override
    public void permissionsWereDenied() {

    }

    private String getIDNumber(MRZResultModel model) {
        if (model.optionals == null) {
            return model.documentNumber;
        }
        return model.optionals[0];
    }

    private String getNationality(MRZResultModel model) {
        if (model.nationality == null) {
            return model.issuingCountry;
        }
        return model.nationality;
    }

    private void startMRZ(String mrzRegistrationKey){
        MRZScanner mrzScanner = (MRZScanner) getSupportFragmentManager().findFragmentById(R.id.mrz_fragment);
        mrzScanner.setScannerType(ScannerType.SCANNER_TYPE_MRZ);
        MRZScanner.setDateFormat("dd/MM/yyyy");
        registerLicenseKey(this,mrzRegistrationKey);

    }

    private String getDocumentIDBasedOnCountry(MRZResultModel resultModel) {
        String[] nationality = new String[0];
        try {
            nationality = convertToStringArray(requestInfoParams.getJSONObject(Constant.REQUEST_INFO).getJSONArray(ValidIssuingCountries));

            String personalNumber = getIDNumber(resultModel);
            if (nationality.equals("PAK")) {
                return resultModel.document_number_check_digit().trim() + personalNumber;
            } else if (nationality.equals("CMR") || nationality.equals("OMR")) {
                return resultModel.documentNumber;
            } else if (nationality.equals("SEN")) {
                return resultModel.documentNumber.trim() + personalNumber.substring(0, (personalNumber.length() - 1)).trim();
            }
            return personalNumber;
        } catch (Exception e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
        return null;
    }

    private void setMRZControlParams(MRZScanner scanner) {
        try {
            JSONObject requestInfo = requestInfoParams.getJSONObject(Constant.REQUEST_INFO);

            if (requestInfo.has(IDScan)) {
                MRZScanner.setIDActive(requestInfo.getBoolean(IDScan));
            }
            if (requestInfo.has(VisaScan)) {
                MRZScanner.setVisaActive(requestInfo.getBoolean(VisaScan));
            }
            if (requestInfo.has(PassportScan)) {
                MRZScanner.setPassportActive(requestInfo.getBoolean(PassportScan));
            }
            if (requestInfo.has(EnableGalleryScan)) {
                scanner.enableScanFromGallery(requestInfo.getBoolean(EnableGalleryScan));
            }
            if (requestInfo.has(CAMERA_INCOMPATIBLE)) {
                MRZScanner.warnIncompatibleCamera(requestInfo.getBoolean(CAMERA_INCOMPATIBLE));
            }
            if (requestInfo.has(ValidIssuingCountries)) {
                MRZScanner.setValidCountryCodes(Constant.convertToStringArray(requestInfo.getJSONArray(ValidIssuingCountries)));
            }
            if (requestInfo.has(Vibration)) {
                MRZScanner.setEnableVibrationOnSuccess(requestInfo.getBoolean(Vibration));
            }
            if (requestInfo.has(ScanFromGallery)) {
                MRZScanner.scanFromGallery(this, this);
            }
            if (requestInfo.has(ToggleFlash)) {
                scanner.toggleFlash(requestInfo.getBoolean(ToggleFlash));
            }
            if (requestInfo.has(DATE_FORMAT)) {
                MRZScanner.setDateFormat(requestInfoParams.getString(DATE_FORMAT));
            }

        } catch (Exception e) {
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }

    }

    private RequestParameter setLicenseKeyParameter() {
        RequestParameter requestParams = new RequestParameter();
        try {
            requestParams.requestInfo.licenseKey = requestInfoFields.getString(getString(R.string.FIELD_LICENSE_KEY));
            requestParams.requestInfo.packageID = packageName;
            return requestParams;
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }

        return null;
    }

    private void getSavedInstanceParams()  {
        try {
            Bundle bundle = getIntent().getExtras();
            this.requestInfoParams = new JSONObject(bundle.getString(Constant.REQUEST_INFO));
            requestInfoFields = this.requestInfoParams.getJSONObject(Constant.REQUEST_INFO);
            packageName = bundle.getString(Constant.PACKAGE_NAME);
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
    }

    private JSONObject createJSONObject(String fieldName, String data)  {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(NAME, fieldName);
            jsonObject.put(VALUE, data);
            return jsonObject;
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
       return jsonObject;
    }

    private JSONObject setPayload(String status, String errorCode, String errorDescription, JSONArray data) {
        JSONObject _status = new JSONObject();
        JSONObject payload = new JSONObject();
        try {
            _status.put(Constant.STATUS, status);
            _status.put(Constant.ERROR_CODE, errorCode);
            _status.put(Constant.ERROR_DESCRIPTION, errorDescription);

            payload.put(Constant.STATUS_INFO, _status);
            payload.put(Constant.FIELDS, data);
        }catch (Exception ex){
            goBackToPrevious("99",Constant.errorMessage.get(99));
        }
        return payload;

    }


    private void registerLicenseKey(final Context context, final String key) {
        MRZScanner.registerWithLicenseKey(context, key, new MRZLicenceResultListener() {
            @Override
            public void onRegisterWithLicenceResult(int result, @Nullable String errorMessage) {
                if (errorMessage != null) {
                  goBackToPrevious("94",Constant.errorMessage.get(94));
                }
            }

        });
    }

    public String getFieldsValue(String fieldName){
        for(Fields field : requestInfoReponse.fields){
            if(field.name.equals(fieldName)){
                return field.value;
            }
        }
        return null;
    }

//    public static void setRequestInfoParams(JSONObject _requestInfoParams){
//        requestInfoParams = _requestInfoParams;
//    }
//
//    public static Intent startActivity(Context context){
//            Intent intent = new Intent(context, ScanActivity.class);
//            activityContext = context;
//
//            return intent;
//    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static class Builder {

        private JSONObject requestInfoParams = null;

        public ScanActivity.Builder setRequestInfoParams(JSONObject requestInfoParams) {
            this.requestInfoParams = requestInfoParams;
            return this;
        }

        public Intent build(Context context) {
            Intent intent = new Intent(context, ScanActivity.class);
            intent.putExtra(Constant.REQUEST_INFO, requestInfoParams.toString());
            intent.putExtra(Constant.PACKAGE_NAME,context.getPackageName());
            return intent;
        }


    }
}
