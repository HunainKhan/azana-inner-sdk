package com.azana.MRZ.Scan.api;


import com.azana.MRZ.Scan.commons.models.RequestInfoParam;
import com.azana.MRZ.Scan.commons.models.RequestParameter;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiConfig {

    @POST("api/ValidateLicenseKey")
    Call<RequestInfoParam> validateLicenseKey(@Body RequestParameter requestParam);

}
