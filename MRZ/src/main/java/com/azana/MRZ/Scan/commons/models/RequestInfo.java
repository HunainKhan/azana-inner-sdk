package com.azana.MRZ.Scan.commons.models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestInfo implements Serializable {

    @SerializedName("licenseKey")
    public String  licenseKey;

    @SerializedName("packageID")
    public String packageID;
}
