package com.azana.commons.models;

public enum ScanSide {
        FRONT,
        BACK,
        BOTH
}
